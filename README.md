# 통합재난경보발령시스템 #

우리의 프로젝트에서는 대한민국 통합재난경보발령시스템(Korea Integrated Emergency Alert System)의 발전을 위한 참조모델의 연구개발이 진행중에 있다. 당신은 이 페이지에서 우리의 프로젝트를 이해하기 위해 필요한 자료들을 얻을 수 있다. 프로젝트에 대한 더 상세한 소개와 관련 작업 결과물(문서)은 위키 페이지(http://alertwiki.uos.ac.kr)에서 열람할 수 있다.

## 1. 필요 환경 ##



## 2. 참고문서 ##

* 우리 프로젝트와 관련된 모든 참고문서는 [구글 드라이브(Root)](https://drive.google.com/folderview?id=0B2mOw8eNDJEGVTdYaTZtM0lESmM&usp=sharing)에서 열람할 수 있다.

#### [ 2015 공통경보프로토콜 기술설명회 ] ####
 * [기술설명회 자료](https://drive.google.com/folderview?id=0B_zNG8ZKU_9-M3I2YWlSMk1kbUk&usp=sharing)


#### [ CAP/EDXL 표준 (외부 링크) ] ####

* [CAP v1.2 OASIS 표준문서](http://docs.oasis-open.org/emergency/cap/v1.2/CAP-v1.2-os.pdf)
* [CAP Feeds v1.0 OASIS 표준문서](http://docs.oasis-open.org/emergency-adopt/cap-feeds/v1.0/cap-feeds-v1.0.pdf)
* [CAP Elements v1.0 OASIS 표준문서](http://docs.oasis-open.org/emergency-adopt/cap-elements/v1.0/cn01/cap-elements-v1.0-cn01.pdf)
* [EDXL-DE v2.0 OASIS 표준문서](http://docs.oasis-open.org/emergency/edxl-de/v2.0/cs02/edxl-de-v2.0-cs02.pdf)
* [EDXL-CT v1.0 OASIS 표준문서](http://docs.oasis-open.org/emergency/edxl-ct/v1.0/csd03/edxl-ct-v1.0-csd03.pdf)
* [EDXL-SitRep v1.0 OASIS 표준문서](http://docs.oasis-open.org/emergency/edxl-sitrep/v1.0/cs01/edxl-sitrep-v1.0-cs01.pdf)

#### [ 국내 표준 문서 (외부 링크, TTA 로그인 필요) ] ####

* ["통합경보시스템을 위한 공통경보프로토콜 프로파일" TTAK.OT-06.0055/R1](http://www.tta.or.kr/data/ttas_view.jsp?rn=1&rn1=Y&rn2=&rn3=&nowpage=1&pk_num=TTAK.OT-06.0055%2FR1&standard_no=OT-06.0055&kor_standard=&publish_date=&section_code=&order=publish_date&by=desc&nowSu=1&totalSu=2&acode1=&acode2=&scode1=&scode2=)
* ["지상파 디지털 멀티미디어 방송(DMB) 재난 경보 서비스" TTAK.KO-07.0046/R5](http://www.tta.or.kr/data/ttas_view.jsp?rn=1&rn1=Y&rn2=&rn3=&nowpage=1&pk_num=TTAK.KO-07.0046%2FR5&standard_no=07.0046&kor_standard=&publish_date=&section_code=&order=publish_date&by=desc&nowSu=1&totalSu=6&acode1=&acode2=&scode1=&scode2=)
* ["이기종 경보 시스템 서버와 통합 재난 경보 게이트웨이 연계 프로토콜" TTAK.KO-09.0085/R1](http://www.tta.or.kr/data/ttas_view.jsp?rn=1&rn1=Y&rn2=&rn3=&nowpage=1&pk_num=TTAK.KO-09.0085%2FR1&standard_no=&kor_standard=%C0%CC%B1%E2%C1%BE+%B0%E6%BA%B8&publish_date=&section_code=&order=publish_date&by=desc&nowSu=1&totalSu=2&acode1=&acode2=&scode1=&scode2=)
* ["옥내경보방송시스템" TTAK.KO-06.0363](http://www.tta.or.kr/data/ttas_view.jsp?rn=1&rn1=Y&rn2=&rn3=&nowpage=1&pk_num=TTAK.KO-06.0363&standard_no=&kor_standard=%BF%C1%B3%BB%B0%E6%BA%B8&publish_date=&section_code=&order=publish_date&by=desc&nowSu=1&totalSu=1&acode1=&acode2=&scode1=&scode2=)
* ["경보 표출 방법" TTAK.KO-06.0364/R1](http://www.tta.or.kr/data/ttas_view.jsp?rn=1&rn1=Y&rn2=&rn3=&nowpage=1&pk_num=TTAK.KO-06.0364%2FR1&standard_no=&kor_standard=%B0%E6%BA%B8+%C7%A5%C3%E2&publish_date=&section_code=&order=publish_date&by=desc&nowSu=1&totalSu=2&acode1=&acode2=&scode1=&scode2=)
* ["LTE 망에서 재난문자 서비스 제공을 위한 요구사항 및 메시지 형식" KCS.KO-06.0263/R1](http://www.tta.or.kr/data/ttas_view.jsp?rn=1&rn1=Y&rn2=&rn3=&nowpage=1&pk_num=TTAK.KO-06.0263%2FR1&standard_no=&kor_standard=lte+%B8%C1&publish_date=&section_code=&order=publish_date&by=desc&nowSu=1&totalSu=2&acode1=&acode2=&scode1=&scode2=)