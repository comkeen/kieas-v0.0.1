package kr.ac.uos.ai.ieas.main;

import kr.ac.uos.ai.ieas.alertSystem.alertSystemController.AlertSystemController;


public class AlertSystemMain
{
	public AlertSystemMain()
	{
		new AlertSystemController();
	}
	
	public static void main(String[] args)
	{
		new AlertSystemMain();
	}
}
